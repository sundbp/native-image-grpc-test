(def project 'grpc-test/monorepo)
(def version "0.1.0-SNAPSHOT")

(task-options!
 pom {:project     project
      :version     version
      :description "A test example of a grpc API using a native-image client."
      :url         "https://gitlab.com/sundbp/native-image-grpc-test"
      :scm         {:url "http://gitlab.com/sundbp/native-image-grpc-test"}
      :license     {"MIT" "???"}}
 push {:ensure-branch nil
       :ensure-clean nil})


;; common dependencies
(merge-env! :dependencies '[[org.clojure/clojure "1.9.0"]
                            ;; Logging
                            [org.clojure/tools.logging "0.4.0"]])

;; util fns
(def merge-with-vec
  (partial
    merge-with
    (comp vec concat)))


(defn merge-map! [m]
  (->> m
       seq
       (apply concat)
       vec
       (apply merge-env!)))


;;;;;; dev repl fns

(defn dev-env [deployment]
  "no cidr, repl bits"
  {:resource-paths #{(str deployment "/dev")}
   :dependencies '[[org.clojure/tools.namespace "0.2.11"]
                   [org.clojure/java.classpath "0.2.3"]]
   :repl-options {:init-ns (symbol (str deployment ".user"))}})


(defn dev
  ([deployment port] (dev deployment port false))
  ([deployment port headless?]
   (merge-map! (dev-env deployment))
   (merge-map! {:dependencies '[[com.cemerick/nrepl "0.3.0-RC1"]]})
   (let [repl-fn (if headless?
                   (comp (repl :server true
                               :bind "0.0.0.0"
                               :port port
                               :init-ns (symbol (str deployment ".user")))
                         (wait))
                   (repl :bind "0.0.0.0"
                         :port port
                         :init-ns (symbol (str deployment ".user"))))]
     (println (str "Starting nREPL on 0.0.0.0:" port))
     (comp (javac) repl-fn))))


;;;;;; uberjar related tasks

(defn- get-pom-options []
  (-> #'pom meta :task-options))


(defn- manifest [version]
  (let [now (pr-str (java.util.Date.))
        build-version (or (System/getenv "BUILD_VERSION")
                          (.substring now (inc (.indexOf now "\"")) (.lastIndexOf now ".")))
        build-number (or (System/getenv "BUILD_NUMBER") build-version)]
    {"Implementation-Version" (str version "." build-version)
     "Build-Version" build-version
     "Build-Number" build-number}))


(deftask standalone
  "Creates a standalone uberjar with a manifest."
  [m main NS sym "Main namespace to AOT"
   b base-name NAME str "base name for uberjar"]
  (let [{:keys [project version]} (get-pom-options)
        classifier "standalone"]
    (comp (aot :namespace #{main})
          (pom :classifier classifier)
          (uber)
          (jar :manifest (manifest version)
               :main main
               :file (str base-name "-" version "-" classifier ".jar")))))


;;;;;; common dependency parts

(def logback
  {:dependencies '[[ch.qos.logback/logback-classic "1.2.3" :exclusions [org.slf4j/slf4j-api]]
                   [org.slf4j/jul-to-slf4j "1.7.25"]
                   [org.slf4j/jcl-over-slf4j "1.7.25"]
                   [org.slf4j/log4j-over-slf4j "1.7.25"]]})

;;;;;; api-service

(def api-env
  (merge-with-vec logback
                  {:resource-paths ["server/src"]
                   :source-paths   ["protos/api/gen/pb-java"]
                   :dependencies   '[;; gRPC
                                     [com.google.protobuf/protobuf-java "3.5.1"]
                                     [io.grpc/grpc-netty "1.12.0"]
                                     [io.grpc/grpc-protobuf "1.12.0"]
                                     [io.grpc/grpc-stub "1.12.0"]]}))


(deftask merge-api-env!
  []
  (merge-map! api-env)
  identity)


(deftask api-repl []
  (merge-api-env!)
  (dev "server" 7784))


(deftask api-uberjar []
  (merge-api-env!)
  (comp (javac)
        (standalone :base-name "api-server" :main 'grpc-test.server.main)
        (target)))


;;; cli-client

(def cli-client-env
  (merge-with-vec logback
                  {:resource-paths ["client/src"]
                   :source-paths   ["protos/api/gen/pb-java"]
                   :dependencies   '[;; cli
                                     [org.clojure/tools.cli "0.3.7"]
                                     ;; gRPC
                                     [com.google.protobuf/protobuf-java "3.5.1"]
                                     [io.grpc/grpc-netty "1.12.0"]
                                     [io.grpc/grpc-protobuf "1.12.0"]
                                     [io.grpc/grpc-stub "1.12.0"]]}))


(deftask merge-cli-client-env!
  []
  (merge-map! cli-client-env)
  identity)


(deftask cli-client-repl []
  (merge-cli-client-env!)
  (dev "client" 7783))


(deftask cli-client-uberjar []
  (merge-cli-client-env!)
  (comp (javac)
        (aot :all true)
        (standalone :base-name "cli-client" :main 'grpc-test.client.main)
        (target)))
