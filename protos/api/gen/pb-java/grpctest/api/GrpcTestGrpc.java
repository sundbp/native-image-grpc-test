package grpctest.api;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.9.2-SNAPSHOT)",
    comments = "Source: api/api.proto")
public final class GrpcTestGrpc {

  private GrpcTestGrpc() {}

  public static final String SERVICE_NAME = "grpctest.api.GrpcTest";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getPingMethod()} instead. 
  public static final io.grpc.MethodDescriptor<grpctest.api.PingRequest,
      grpctest.api.PingResponse> METHOD_PING = getPingMethod();

  private static volatile io.grpc.MethodDescriptor<grpctest.api.PingRequest,
      grpctest.api.PingResponse> getPingMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<grpctest.api.PingRequest,
      grpctest.api.PingResponse> getPingMethod() {
    io.grpc.MethodDescriptor<grpctest.api.PingRequest, grpctest.api.PingResponse> getPingMethod;
    if ((getPingMethod = GrpcTestGrpc.getPingMethod) == null) {
      synchronized (GrpcTestGrpc.class) {
        if ((getPingMethod = GrpcTestGrpc.getPingMethod) == null) {
          GrpcTestGrpc.getPingMethod = getPingMethod = 
              io.grpc.MethodDescriptor.<grpctest.api.PingRequest, grpctest.api.PingResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "grpctest.api.GrpcTest", "Ping"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  grpctest.api.PingRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  grpctest.api.PingResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GrpcTestMethodDescriptorSupplier("Ping"))
                  .build();
          }
        }
     }
     return getPingMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GrpcTestStub newStub(io.grpc.Channel channel) {
    return new GrpcTestStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GrpcTestBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new GrpcTestBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GrpcTestFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new GrpcTestFutureStub(channel);
  }

  /**
   */
  public static abstract class GrpcTestImplBase implements io.grpc.BindableService {

    /**
     */
    public void ping(grpctest.api.PingRequest request,
        io.grpc.stub.StreamObserver<grpctest.api.PingResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getPingMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getPingMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                grpctest.api.PingRequest,
                grpctest.api.PingResponse>(
                  this, METHODID_PING)))
          .build();
    }
  }

  /**
   */
  public static final class GrpcTestStub extends io.grpc.stub.AbstractStub<GrpcTestStub> {
    private GrpcTestStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GrpcTestStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GrpcTestStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GrpcTestStub(channel, callOptions);
    }

    /**
     */
    public void ping(grpctest.api.PingRequest request,
        io.grpc.stub.StreamObserver<grpctest.api.PingResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPingMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class GrpcTestBlockingStub extends io.grpc.stub.AbstractStub<GrpcTestBlockingStub> {
    private GrpcTestBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GrpcTestBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GrpcTestBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GrpcTestBlockingStub(channel, callOptions);
    }

    /**
     */
    public grpctest.api.PingResponse ping(grpctest.api.PingRequest request) {
      return blockingUnaryCall(
          getChannel(), getPingMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class GrpcTestFutureStub extends io.grpc.stub.AbstractStub<GrpcTestFutureStub> {
    private GrpcTestFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GrpcTestFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GrpcTestFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GrpcTestFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<grpctest.api.PingResponse> ping(
        grpctest.api.PingRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getPingMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_PING = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GrpcTestImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GrpcTestImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PING:
          serviceImpl.ping((grpctest.api.PingRequest) request,
              (io.grpc.stub.StreamObserver<grpctest.api.PingResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class GrpcTestBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    GrpcTestBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return grpctest.api.APIProtos.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("GrpcTest");
    }
  }

  private static final class GrpcTestFileDescriptorSupplier
      extends GrpcTestBaseDescriptorSupplier {
    GrpcTestFileDescriptorSupplier() {}
  }

  private static final class GrpcTestMethodDescriptorSupplier
      extends GrpcTestBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    GrpcTestMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GrpcTestGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GrpcTestFileDescriptorSupplier())
              .addMethod(getPingMethod())
              .build();
        }
      }
    }
    return result;
  }
}
