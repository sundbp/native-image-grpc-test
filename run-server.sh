#!/bin/bash

mkdir -p bin
if [ ! -x bin/boot ];then
    echo "-------> installing boot build tool"
    cd bin && curl -fsSLo boot https://github.com/boot-clj/boot-bin/releases/download/latest/boot.sh && chmod 755 boot && cd -
fi

echo "-------> create api-service uberjar.."
./bin/boot api-uberjar
cp target/api-*standalone* .

java -jar api-server-0.1.0-SNAPSHOT-standalone.jar
