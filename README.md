# native-image-grpc-test

This is an example attempt of using a native-image clojure based gRPC cli client with a gRPC based service. The detail that it is clojure based shouldn't really matter in the sense of native-image usage.

## Requirements

- graal rc6

## How to replicate

In one terminal window run:

```bash
$ ./run-server.sh
```

In another terminal window run:

```bash
$ export GRAALJDK_JAVA_HOME=/path/to/your/labsjdk-java-home
$ ./create-exe.sh
$ ./cli-client example-message
```

And now we actually have a successful result and a working binary!!!!

## Details

It's probably easiest to read the `create-exe.sh` script to see what is going on. You can also explode the uberjar files (the standalone files copied to current dir by the scripts), that way you see all classes etc included.

