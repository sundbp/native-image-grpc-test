(ns grpc-test.server.main
  (:gen-class)
  (:require [clojure.tools.logging :as log])
  (:import [io.grpc Server ServerBuilder ServerInterceptor ServerInterceptors
            Metadata Metadata$Key ServerServiceDefinition BindableService]
           [io.grpc.stub StreamObserver]
           [grpctest.api PingRequest PingResponse]))

(set! *warn-on-reflection* true)

;;; grpc

(defn- make-service
  []
  (proxy [grpctest.api.GrpcTestGrpc$GrpcTestImplBase] []
    (ping [^PingRequest request ^StreamObserver response]
      (log/debug ::ping request)
      (.onNext response (-> (PingResponse/newBuilder)
                            (.setMessage (format "Server received: '%s'" (.getMessage request)))
                            .build))
      (.onCompleted response))))


(defn start-server ^Server
  [port]
  (log/info ::start-server "starting server on port" port)
  (let [builder (ServerBuilder/forPort port)
        service ^grpctest.api.GrpcTestGrpc$GrpcTestImplBase (make-service)
        server  ^Server (-> builder (.addService service) .build)]
    (.start server)
    server))


;;; main

(defn- on-shutdown! [^Runnable callback]
  (.addShutdownHook (Runtime/getRuntime)
                    (Thread. callback)))


(defn- wait-for-exit
  []
  (.join (Thread/currentThread)))


(defn -main [& args]
  ;; perform any work needed to be done BEFORE we start the system here!
  (log/info ::-main "system is starting up..")
  ;;(mount/start)
  (let [server (start-server 5657)]
    (on-shutdown! #(do (log/info ::-main "stopping system...")
                       ;;(mount/stop)
                       (.shutdown server)
                       (log/info ::-main "system stopped successfully."))))
  (log/info ::-main "system started successfully.")
  ;; perform any work needed to be done AFTER the system has started here!
  (wait-for-exit))
