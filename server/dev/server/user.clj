(ns server.user
  (:require [clojure.java.classpath :as classpath]
            [clojure.tools.namespace.repl :as repl]
            [boot.core]
            ;; ensure we get everything else we need via main
            [grpc-test.server.main :as main]))


(defn add-dep
  "Add a dep to project without restarting REPL.
  Remember to quote the dep, i.e. (add-dep '[byte-streams \"0.2.3\"])"
  [dep]
  (boot.core/set-env! :dependencies #(into % [dep])))
