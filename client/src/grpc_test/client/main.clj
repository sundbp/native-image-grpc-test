(ns grpc-test.client.main
  (:gen-class)
  (:require [clojure.tools.cli :as cli]
            [clojure.string :as string]
            [clojure.tools.logging :as log])
  (:import [grpctest.api
            GrpcTestGrpc GrpcTestGrpc$GrpcTestBlockingStub
            PingRequest PingResponse]
           [java.util.concurrent TimeUnit]))

(set! *warn-on-reflection* true)

;;; grpc

(defn- create-client
  [^String host port]
  (let [channel (-> (io.grpc.ManagedChannelBuilder/forAddress host (int port))
                    (.usePlaintext true)
                    .build)]
    {:channel channel
     :stub    (GrpcTestGrpc/newBlockingStub channel)}))


;;; API calls

(defn ping
  [{:keys [server port]} message]
  (log/infof "Pinging server at %s:%d with message '%s'" server port message)
  (let [c (create-client server port)]
    (try
      (let [response (.ping ^GrpcTestGrpc$GrpcTestBlockingStub (:stub c)
                            (-> (PingRequest/newBuilder) (.setMessage ^String message) .build))]
        (println (format "Got ping response back from server: '%s'" (.getMessage response))))
      (finally
        (-> (.shutdown ^io.grpc.ManagedChannel (:channel c))
            (.awaitTermination 5 TimeUnit/SECONDS))))))


;;; command line parsing

(def cli-input [["-h" "--help"   "Display this help."
                 :default false]
                ["-s" "--server SERVER" "the host with the server running"
                 :default "127.0.0.1"
                 :parse-fn identity]
                ["-p" "--port HOST" "the host with the server running"
                 :default 5657
                 :parse-fn #(Integer/parseInt %)]])


(defn- print-help
  [opts]
  (println "Usage: cli-client [OPTIONS] message")
  (println "Where OPTIONS are:")
  (println (:summary opts))
  (println ""))


;;; main entrypoint

(defn- on-shutdown! [^Runnable callback]
  (.addShutdownHook (Runtime/getRuntime)
                    (Thread. callback)))


(defn -main [& args]
  ;; perform any work needed to be done BEFORE we start the system here!
  (log/info ::-main "system is starting up..")
  ;;(mount/start)
  (on-shutdown! #(do (log/info ::-main "stopping system...")
                     ;;(mount/stop)
                     (log/info ::-main "system stopped successfully.")))
  (log/info ::-main "system started successfully.")
  ;; perform any work needed to be done AFTER the system has started here!
  (let [opts (cli/parse-opts args cli-input)
        message (string/join " " (:arguments opts))]
    (if (or (get-in opts [:options :help])
            (string/blank? message))
      (print-help opts)
      (ping (:options opts) message))))
