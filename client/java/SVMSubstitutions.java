import com.oracle.svm.core.annotate.Alias;
import com.oracle.svm.core.annotate.RecomputeFieldValue;
import com.oracle.svm.core.annotate.RecomputeFieldValue.Kind;
import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.JdkLoggerFactory;

import static io.netty.buffer.Unpooled.buffer;
import static io.netty.buffer.Unpooled.directBuffer;
import static io.netty.buffer.Unpooled.unreleasableBuffer;
import static io.netty.handler.codec.http.HttpConstants.CR;
import static io.netty.handler.codec.http.HttpConstants.LF;

import static io.netty.util.CharsetUtil.UTF_8;

import io.netty.buffer.Unpooled;
import io.netty.buffer.ByteBuf;
import jdk.vm.ci.meta.MetaAccessProvider;
import jdk.vm.ci.meta.ResolvedJavaField;

@TargetClass(io.netty.util.internal.logging.InternalLoggerFactory.class)
final class Target_io_netty_util_internal_logging_InternalLoggerFactory {
    @Substitute
    private static InternalLoggerFactory newDefaultFactory(String name) {
        return JdkLoggerFactory.INSTANCE;
    }
}

@TargetClass(className = "io.netty.util.internal.PlatformDependent0")
final class Target_io_netty_util_internal_PlatformDependent0 {
    @Alias @RecomputeFieldValue(kind = Kind.FieldOffset, //
                                declClassName = "java.nio.Buffer", //
                                name = "address") //
                                private static long ADDRESS_FIELD_OFFSET;
}

@TargetClass(className = "io.netty.util.internal.CleanerJava6")
final class Target_io_netty_util_internal_CleanerJava6 {
    @Alias @RecomputeFieldValue(kind = Kind.FieldOffset, //
                                declClassName = "java.nio.DirectByteBuffer", //
                                name = "cleaner") //
                                private static long CLEANER_FIELD_OFFSET;
}

@TargetClass(className = "io.netty.util.internal.shaded.org.jctools.util.UnsafeRefArrayAccess")
final class Target_io_netty_util_internal_shaded_org_jctools_util_UnsafeRefArrayAccess {
    @Alias @RecomputeFieldValue(kind = Kind.ArrayIndexShift, declClass = Object[].class) //
    public static int REF_ELEMENT_SHIFT;
}

@TargetClass(className = "com.google.protobuf.UnsafeUtil")
final class Target_com_google_protobuf_UnsafeUtil {

    @Substitute
    static sun.misc.Unsafe getUnsafe() {
        return null;
    }
}

public class SVMSubstitutions {
}

@TargetClass(className = "io.netty.handler.codec.http.HttpObjectEncoder")
final class Target_io_netty_handler_codec_http_HttpObjectEncoder {
     @Alias @RecomputeFieldValue(kind = Kind.Custom, declClass = Recomputer1.class)
     private static ByteBuf CRLF_BUF;
     @Alias @RecomputeFieldValue(kind = Kind.Custom, declClass = Recomputer2.class)
     private static ByteBuf ZERO_CRLF_CRLF_BUF;
}

final class Recomputer1 implements RecomputeFieldValue.CustomFieldValueComputer {

	@Override
	public Object compute(MetaAccessProvider metaAccess, ResolvedJavaField original, ResolvedJavaField annotated,
			Object receiver) {
		return Unpooled.unreleasableBuffer(buffer(2).writeByte(CR).writeByte(LF));
	}
}

final class Recomputer2 implements RecomputeFieldValue.CustomFieldValueComputer {
    private static final byte[] ZERO_CRLF_CRLF = { '0', CR, LF, CR, LF };

    @Override
    public Object compute(MetaAccessProvider metaAccess, ResolvedJavaField original, ResolvedJavaField annotated,
                          Object receiver) {
        return Unpooled.unreleasableBuffer(buffer(ZERO_CRLF_CRLF.length)
                                           .writeBytes(ZERO_CRLF_CRLF));
    }
}

@TargetClass(className = "io.netty.handler.codec.http2.Http2CodecUtil")
final class Target_io_netty_handler_codec_http2_Http2CodecUtil {
     @Alias @RecomputeFieldValue(kind = Kind.Custom, declClass = Recomputer3.class)
     private static ByteBuf CONNECTION_PREFACE;
     @Alias @RecomputeFieldValue(kind = Kind.Custom, declClass = Recomputer4.class)
     private static ByteBuf EMPTY_PING;
}

final class Recomputer3 implements RecomputeFieldValue.CustomFieldValueComputer {

	@Override
	public Object compute(MetaAccessProvider metaAccess, ResolvedJavaField original, ResolvedJavaField annotated,
			Object receiver) {
      return Unpooled.unreleasableBuffer(buffer(24).writeBytes("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n".getBytes(UTF_8))).asReadOnly();
	}
}

final class Recomputer4 implements RecomputeFieldValue.CustomFieldValueComputer {
    @Override
    public Object compute(MetaAccessProvider metaAccess, ResolvedJavaField original, ResolvedJavaField annotated,
                          Object receiver) {
        return Unpooled.unreleasableBuffer(buffer(8).writeZero(8)).asReadOnly();
    }
}

@TargetClass(className = "io.netty.handler.codec.http2.DefaultHttp2FrameWriter")
final class Target_io_netty_handler_codec_http2_DefaultHttp2FrameWriter {
    @Alias @RecomputeFieldValue(kind = Kind.Custom, declClass = Recomputer5.class)
    private static ByteBuf ZERO_BUFFER;
}

final class Recomputer5 implements RecomputeFieldValue.CustomFieldValueComputer {
    public static final short MAX_UNSIGNED_BYTE = 0xff;

    @Override
    public Object compute(MetaAccessProvider metaAccess, ResolvedJavaField original, ResolvedJavaField annotated,
                          Object receiver) {
        return Unpooled.unreleasableBuffer(buffer(MAX_UNSIGNED_BYTE).writeZero(MAX_UNSIGNED_BYTE)).asReadOnly();
    }
}
