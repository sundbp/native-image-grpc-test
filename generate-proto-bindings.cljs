#!/usr/bin/env planck
;; NOTE: to run this script please install the planck REPL
;;  Instructions can be found here: http://planck-repl.org/

(ns permafrost.generate-proto-bindings
  (:require [planck.core :as pcore]
            [planck.shell :as shell]
            [goog.string :as gstring]
            [goog.string.format]))

(def protoc-image-tag "1")

(defn- sh
  [cmd]
  (apply shell/sh (clojure.string/split cmd #"\s+")))


(defn- sh-echo
  [cmd]
  (println cmd)
  (let [result (sh cmd)]
    (when-not (clojure.string/blank? (:out result))
      (println "stdout:")
      (print (:out result)))
    (when-not (clojure.string/blank? (:err result))
      (println "stderr:")
      (print (:err result)))
    (when-not (zero? (:exit result))
      (println (gstring/format "exited with code %d" (:exit result))))
    result))

(defn- up-one
  [service-dir]
  (let [parts (-> service-dir (clojure.string/split #"\/") drop-last)]
    (clojure.string/join "/" parts)))


(defn- update-bindings-for-lang
  [service-dir lang]
  (sh (gstring/format "mkdir -p %s/gen/pb-%s" service-dir lang))
  (let [service-name (-> service-dir (clojure.string/split #"\/") last)
        path (up-one service-dir)]
    (shell/with-sh-dir path
      (let [pwd (-> (sh "pwd") :out clojure.string/trim)
            docker-cmd (gstring/format "docker run --rm -v %s:/defs namely/protoc-all:latest -d /defs/%s -l %s -i /defs -o /defs/%s/gen/pb-%s"
                                       pwd
                                       service-name
                                       lang
                                       service-name
                                       lang)]
        (sh-echo docker-cmd)))))


(defn- update-proto-bindings
  [service-dir]
  (let [langs (-> (pcore/slurp (str service-dir, "/.protolangs"))
                  (clojure.string/split #"\s+"))]
    (doseq [lang langs]
      (println (gstring/format "\ngenerating output for %s:" lang))
      (update-bindings-for-lang service-dir lang))))


;;; main entry point of script
(doseq [service-dir (-> (sh "find protos -maxdepth 1 -mindepth 1 -type d")
                        :out
                        (clojure.string/split #"\s+"))]
  (println (gstring/format ">>>>>>> Updating proto bindings for %s:" service-dir))
  (update-proto-bindings service-dir))

(println "done.")
