#!/bin/bash

mkdir -p bin
if [ ! -x bin/boot ];then
    echo "-------> installing boot build tool"
    cd bin && curl -fsSLo boot https://github.com/boot-clj/boot-bin/releases/download/latest/boot.sh && chmod 755 boot && cd -
fi

echo "-------> create cli-tool uberjar.."
./bin/boot cli-client-uberjar
cp target/cli-client*standalone* .

echo "-------> set graal jdk JAVA_HOME"
export JAVA_HOME=$GRAALJDK_JAVA_HOME
echo "JAVA_HOME is now: $JAVA_HOME"
export PATH=$JAVA_HOME/bin:$PATH

echo "-------> compile substitutions"
./compile-substitutions.sh

echo "-------> run native-image"
echo "NOTE: the reason for not using the native-image build server is that it seems to always fail on the 2nd use if I re-created the uberjar!"
native-image --verbose --no-server \
             -Dio.netty.noUnsafe=true \
             -H:+ReportUnsupportedElementsAtRuntime \
             -H:ReflectionConfigurationFiles=./reflectconfigs/netty \
             -H:Name=cli-client \
             --delay-class-initialization-to-runtime=io.netty.handler.ssl.JdkNpnApplicationProtocolNegotiator,io.netty.handler.ssl.ReferenceCountedOpenSslEngine \
             -cp cli-client-0.1.0-SNAPSHOT-standalone.jar:client/java \
             grpc_test.client.main
