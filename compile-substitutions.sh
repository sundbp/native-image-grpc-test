#!/bin/bash

CP=$(./bin/boot merge-cli-client-env! show -c)
CP=$CP:$JAVA_HOME/jre/lib/svm/builder/svm.jar:$JAVA_HOME/jre/lib/jvmci/jvmci-api.jar

echo "using classpath:\n$CP"

javac -cp $CP client/java/SVMSubstitutions.java
